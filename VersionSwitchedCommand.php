<?php

/**
 * @file
 * This file contains the functions needed to run versioned commands.
 */

/**
 * Search for the cli name by walking up the specified path.
 *
 * @param string $dir
 *   The starting directory.
 * @param string $version_env
 *   The version environment variable name.
 * @param string $cli_env
 *   The cli environment variable name.
 * @param string $cmd_prefix
 *   The base command name or prefix.
 * @param string $default_cli
 *   The default return value if nothing is found.
 *
 * @return string|null
 *   The cli name.
 */
function get_cli_name(string $dir, string $version_env, string $cli_env, string $cmd_prefix, string $default_cli = NULL) {
  if ($ret = getenv($cli_env)) {
    return trim($ret);
  }

  if ($ret = getenv($version_env)) {
    return $cmd_prefix . trim($ret);
  }

  $dir = realpath($dir);

  while ($dir && $dir !== '/') {
    if ($filename = realpath($dir . '/.git/config')) {
      $git_config = parse_ini_file($filename, TRUE, INI_SCANNER_RAW);

      if ($git_config[$cmd_prefix]['cli'] ?? NULL) {
        return trim($git_config[$cmd_prefix]['cli']);
      }

      if ($git_config[$cmd_prefix]['version'] ?? NULL) {
        return $cmd_prefix . trim($git_config[$cmd_prefix]['version']);
      }
    }

    if ($filename = realpath($dir . '/' . $cli_env)) {
      return trim(file_get_contents($filename));
    }

    if ($filename = realpath($dir . '/' . $version_env)) {
      return $cmd_prefix . trim(file_get_contents($filename));
    }

    if (function_exists('yaml_parse_file') && $filename = realpath($dir . '/preferences.yml')) {
      $preferences = yaml_parse_file($filename) ?? [];

      if ($preferences[$cmd_prefix]['cli'] ?? NULL) {
        return trim($preferences[$cmd_prefix]['cli']);
      }

      if ($preferences[$cmd_prefix]['version'] ?? NULL) {
        return $cmd_prefix . trim($preferences[$cmd_prefix]['version']);
      }
    }

    if ($filename = realpath($dir . '/preferences.ini')) {
      $preferences = parse_ini_file($filename, TRUE, INI_SCANNER_RAW);

      if ($preferences[$cmd_prefix]['cli'] ?? NULL) {
        return trim($preferences[$cmd_prefix]['cli']);
      }

      if ($preferences[$cmd_prefix]['version'] ?? NULL) {
        return $cmd_prefix . trim($preferences[$cmd_prefix]['version']);
      }
    }

    if ($filename = realpath($dir . '/preferences.json')) {
      $preferences = @json_decode(@file_get_contents($filename), TRUE, 999999) ?? [];

      if ($preferences[$cmd_prefix]['cli'] ?? NULL) {
        return trim($preferences[$cmd_prefix]['cli']);
      }

      if ($preferences[$cmd_prefix]['version'] ?? NULL) {
        return $cmd_prefix . trim($preferences[$cmd_prefix]['version']);
      }
    }

    if ($filename = realpath($dir . '/composer.json')) {
      $composer_json = @json_decode(@file_get_contents($filename), TRUE, 999999) ?? [];

      if ($composer_json['extra'][$cmd_prefix]['cli'] ?? NULL) {
        return trim($composer_json['extra'][$cmd_prefix]['cli']);
      }

      if ($composer_json['extra'][$cmd_prefix]['version'] ?? NULL) {
        return $cmd_prefix . trim($composer_json['extra'][$cmd_prefix]['version']);
      }
    }

    $dir = realpath($dir . '/..');
  }

  if ($ret = getenv('DEFAULT_' . $cli_env)) {
    return trim($ret);
  }

  if ($ret = getenv('DEFAULT_' . $version_env)) {
    return $cmd_prefix . trim($ret);
  }

  return $default_cli ?? NULL;
}

/**
 * Run a version switched command.
 *
 * @param string $dir
 *   The starting directory.
 * @param array $argv
 *   The command line arguments.
 * @param string $cmd_prefix
 *   The command name or prefix.
 * @param array $cmd_versions
 *   The allowed version.
 * @param callable $absolutePathCallback
 *   The callback to build the absolute command path.
 */
function run_version_switched_command(string $dir, array $argv, string $cmd_prefix, array $cmd_versions, callable $absolutePathCallback, string $default_cli = NULL) {
  $allowed_cli_names = array_map(function ($version) use ($cmd_prefix) {
    return $cmd_prefix . $version;
  }, $cmd_versions);

  $version_env = strtoupper($cmd_prefix) . '_VERSION';
  $cli_env = strtoupper($cmd_prefix) . '_CLI';

  $a = implode('|', $allowed_cli_names);
  $b = implode('|', $cmd_versions);

  $help_msg = <<<EOL
  The $cmd_prefix version for this directory tree has not been set.

  Usage:

    cd /path/to/project
    git config $cmd_prefix.cli [$a]

  or

    cd /path/to/project
    git config $cmd_prefix.version [$b]

  or

    cd /path/to/nonprojectdir
    echo [$a] > $cli_env

  or

    cd /path/to/nonprojectdir
    echo [$b] > $version_env

  or add one of these to your .bash_profile or .bashrc

    DEFAULT_$cli_env=[$a]
    DEFAULT_$version_env=[$b]

  EOL;

  $full_runname = $argv[0];
  $runname = basename($full_runname);

  if ($runname === $cmd_prefix) {
    $ret = get_cli_name($dir, $version_env, $cli_env, $cmd_prefix, $default_cli);
  }
  else {
    if (in_array($runname, $allowed_cli_names)) {
      $ret = $runname;
    }
    else {
      fwrite(STDERR, "Invalid Filename or Symlink: $full_runname" . PHP_EOL);
      exit(1);
    }
  }

  if ($ret) {
    putenv("$cli_env=$ret");

    $resolved_cli = realpath($absolutePathCallback($ret));

    if (!$resolved_cli) {
      fwrite(STDERR, "Unable to find: $ret" . PHP_EOL);
      exit(1);
    }

    $res = proc_open(implode(' ', array_map('escapeshellarg', [$resolved_cli, ...array_slice($argv, 1)])), [], $pipes, getcwd(), NULL);
    exit($res ? proc_close($res) : 1);
  }
  else {
    fwrite(STDERR, $help_msg . PHP_EOL);
    exit(1);
  }
}
